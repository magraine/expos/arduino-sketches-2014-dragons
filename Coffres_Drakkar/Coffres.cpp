//
// Gestion de l'ensemble des coffres
//
#include "Coffres.h"

// Constructeur
Coffres::Coffres(int nombreCoffres, Coffre _Coffres[]):
  nombreCoffres(nombreCoffres),
  _Coffres(_Coffres)
{
 
}


// Initialise les coffres
// et les verrouille par défaut s'ils ont les conditions pour.
void Coffres::initialiser() {
  for (int n = 0; n < nombreCoffres; n++) {
    _Coffres[n].initialiser();
  } 
}

// Actualiser les coffres
// Change leurs états, gère les verrous et leds
void Coffres::actualiser() {
  for (int n = 0; n < nombreCoffres; n++) {
    _Coffres[n].actualiser();
  }  
}

// Désactive tous les coffres
void Coffres::desactiver() {
  for (int n = 0; n < nombreCoffres; n++) {
    _Coffres[n].desactiver();
  } 
}

// Verrouille tous les coffres
void Coffres::verrouiller() {
  for (int n = 0; n < nombreCoffres; n++) {
    _Coffres[n].verrouiller();
  } 
}

// Test si tous les coffres sont activés
bool Coffres::tousActifs() {
  bool actifs = true;
  for (int n = 0; n < nombreCoffres; n++) {
    actifs &= _Coffres[n].estActif();
  } 
  return actifs;
}

// Test si tous les coffres sont activés ET fermés
bool Coffres::tousActifsEtFermes() {
  bool actifs = true;
  for (int n = 0; n < nombreCoffres; n++) {
    actifs &= _Coffres[n].estActif();
    actifs &= _Coffres[n].estFerme();
  } 
  return actifs;
}
