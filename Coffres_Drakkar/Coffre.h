#ifndef Coffre_h
#define Coffre_h

#include "Arduino.h"
#include "Button.h"
#include "DmxSimple.h"
#include "Color.h"
#include "Chrono.h"

class Coffre {

  public:
    Coffre(int pinD, int pinO, int pinV, int pinL, int pinS, DmxSimpleClass &DmxSimple, int DmxAdresse, Color couleur);

    int pinTestDeclencheur;
    int pinTestOuverture;
    int pinVerrouillage;
    int pinLeds;
    int pinSon;
    
    DmxSimpleClass &DmxSimple;
    int DmxAdresse;
    
    Color couleur;
    
    Chrono chronoVerrouillage;
    unsigned long delaiPourVerrouillage;
    bool deverrouillageActif;
    
    int etat;        // mémoire de l'état du coffre
    bool actif;      // mémoire de l'état d'activation du coffre
    bool signalEmis; // mémoire de l'état d'émission du signal d'ouverture de coffre
    
    Button btnDeclencheur;
    Button btnOuverture;
    
    void initialiser(void);
    void actualiser(void);
    
    void verrouiller(void);
    void deverrouiller(void);
    bool estVerrouille();
    bool estDeverrouille();
    
    void allumerLeds(void);
    void eteindreLeds(void);
    
    void allumerProjecteur(void);
    void eteindreProjecteur(void);
    void colorerProjecteur(Color c);

    void activer(void);
    void desactiver(void);
    bool estActif(void);
 
    bool estOuvert(void);
    bool estFerme(void);
    
    bool declencheurPresent(void);
    bool declencheurAbsent(void);
    
    void allumerSon(void);
    void couperSon(void);
};

#endif
