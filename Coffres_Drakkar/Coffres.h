#ifndef Coffres_h
#define Coffres_h

#include "Arduino.h"
#include "Coffre.h"

// Juste un conteneur de fonctions 
class Coffres {
  public: 
    Coffres(int nombreCoffres, Coffre _Coffres[]);
    
    Coffre * _Coffres;
    int nombreCoffres;
    
    void initialiser();
    void actualiser();
    
    void desactiver();
    
    void verrouiller();
    
    bool tousActifs();
    bool tousActifsEtFermes();
};


#endif
