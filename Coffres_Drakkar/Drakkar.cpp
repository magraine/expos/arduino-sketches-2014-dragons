
//
// Gestion du drakkar
//
#include "Drakkar.h"
#include <Servo.h>


// Constructeur
Drakkar::Drakkar(int pinServo, unsigned long delaiAttente,  LedRGB ledsYeux, Color couleurYeuxAllumes, int pinCapteurBoule, int pinSignalSonore) :
  pinServoLacheurBoule(pinServo),
  delaiAvantLacherBoule(delaiAttente),
  ledsYeux(ledsYeux),
  couleurYeuxAllumes(couleurYeuxAllumes),
  couleurYeuxEteints(0, 0, 0),
  pinCapteurBoule(pinCapteurBoule),
  pinSignalSonore(pinSignalSonore)
{
  angleBouleRetenue    = 150;
  angleBouleLachee     = 70;
  vitesseOuverture     = 5;
  delaiBouleOuvert     = 8000;
  
  //Servo servoLacheurBoule;
  //servoLacheurBoule.attach(pinServoLacheurBoule);
    
  ledsYeux.color(couleurYeuxEteints);
  
  pinMode(pinCapteurBoule, INPUT_PULLUP);
  pinMode(pinSignalSonore, OUTPUT);
}


void Drakkar::initialiser() {
  servoLacheurBoule.attach(pinServoLacheurBoule);
  eteindreYeux();
  retenirBoule();
}

// Teste si la boule est présente à son emplacement, prète à partir.
bool Drakkar::boulePresente() {
   return !digitalRead(pinCapteurBoule); 
}

// Allumer les yeux sur la proue du drakkar
void Drakkar::allumerYeux() {
   ledsYeux.color(couleurYeuxAllumes);
}

// Eteindre les yeux sur la proue du drakkar
void Drakkar::eteindreYeux() {
   ledsYeux.color(couleurYeuxEteints);
}

// maintenir la position fermée de la boule.
void Drakkar::retenirBoule() {
  servoLacheurBoule.write(angleBouleRetenue);
}

// refermer le verrou de la boule dans la bouche du drakkar
void Drakkar::tenirBoule() {
  for (int i = angleBouleLachee; i <= angleBouleRetenue; i += 1) {
    servoLacheurBoule.write(i);
    delay(vitesseOuverture);
  }
}
  
// ouvrir le verrou la boule de la bouche du drakkar
void Drakkar::lacherBoule() {
  for (int i = angleBouleRetenue; i >= angleBouleLachee; i -= 1) {
    servoLacheurBoule.write(i);
    delay(vitesseOuverture);
  }
}

void Drakkar::angleTenueBoule(long angle) {
  servoLacheurBoule.write(angle);
}

// Envoyer la boule (lacher et refermer, avec des temps d'attente)
void Drakkar::envoyerBoule() {
  delay(delaiAvantLacherBoule); 
  lacherBoule();
  delay(delaiBouleOuvert);
  tenirBoule();
}

// Envoyer le déclenchement d'un son
void Drakkar::envoyerSon() {
  digitalWrite(pinSignalSonore, HIGH);
  delay(50);
  digitalWrite(pinSignalSonore, LOW);
}
