#include "Coffre_extensions.h"


Coffre_Feu::Coffre_Feu(int pinD, int pinO, int pinV, int pinL, int pinS, DmxSimpleClass &DmxSimple, int DmxAdresse, Color couleur): 
  Coffre(pinD, pinO, pinV, pinL, pinS, DmxSimple, DmxAdresse, couleur) {};

// Allumer le projecteur
void Coffre_Feu::allumerProjecteur() {
  Coffre::allumerProjecteur();
};
