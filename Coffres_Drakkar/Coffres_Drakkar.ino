#include <Button.h>
#include <DmxSimple.h>
#include <Color.h>
#include <Servo.h>
#include <LedRGB.h>
#include <BarriereIR.h>
#include <Chrono.h>


// Inclussion d'un .h local (même répertoire !)
#include "Coffre.h"            // Gestion générique d'un coffre
#include "Coffre_extensions.h" // Gestion spécifique de certains coffres
#include "Coffres.h"           // Gestion de l'ensemble des coffres
#include "Drakkar.h"           // Gestion du drakkar


// Mode debug ?
bool Debug = false;

// Config DMX
int pinDMX = 3;
int maxChannelDMX = 28;


// Couleurs (spots DMX)
Color couleur_feu   = Color(255, 0, 0);   // Rouge
Color couleur_eau   = Color(0, 50, 255);  // Turquoise
Color couleur_air   = Color(0, 0, 255);   // Bleu
Color couleur_terre = Color(255, 255, 0); // Jaune


int nombreCoffres = 4;

Coffre C_Terre = Coffre(
  48, // Entrée : Declencheur
  50, // Entrée : Ouverture
  51, // Sortie : Verrou
  49, // Sortie : Leds

  46, // Sortie : Son

  DmxSimple,        // Objet Gérant le Dmx
  7,                // Sortie : Adresse DMX du projecteur (n° d'adresse de la 1è chaine, 6 chaines nécessaires par projecteur).
  couleur_terre     // couleur du spot à allumer
);


Coffre C_Eau = Coffre(
  40, // Entrée : Declencheur
  42, // Entrée : Ouverture
  43, // Sortie : Verrou
  41, // Sortie : Leds
  38, // Sortie : Signal ouvert
  DmxSimple,
  21,  // Adresse DMX
  couleur_eau
);


Coffre C_Air = Coffre(
  32, // Entrée : Declencheur
  34, // Entrée : Ouverture
  35, // Sortie : Verrou
  33, // Sortie : Leds
  30, // Sortie : Signal ouvert
  DmxSimple,
  14,  // Adresse DMX
  couleur_air
);


Coffre C_Feu = Coffre(
  24, // Entrée : Declencheur
  26, // Entrée : Ouverture
  27, // Sortie : Verrou
  25, // Sortie : Leds
  22, // Sortie : Signal ouvert
  DmxSimple,
  1,  // Adresse DMX
  couleur_feu
);

// Déclarer un groupe avec tous les coffres
// pour effectuer des actions communes.
Coffre _Coffres[] = { C_Terre, C_Eau, C_Air, C_Feu };
Coffres coffres = Coffres(nombreCoffres, _Coffres); // note : je n'arrive pas à le déclarer en 1 seule ligne !



// Config du Drakkar
LedRGB LedsDrakkar = LedRGB(5, 6, 7);
Color couleur_bleu_drakkar = Color(0, 50, 255);

Drakkar drakkar = Drakkar(
  4,      // Sortie : Servomoteur qui retient la boule
  1000,   // Delai  : Attente après activation et fermeture de tous les coffres, avant de lacher la boule

  LedsDrakkar, // Sorties (led RGB)
  couleur_bleu_drakkar, // Couleur de coloration des yeux du drakkar

  8,   // Entrée : Capteur de présence de la boule
  9    // Sortie : Impulsion pour déclencher un son
);


Chrono chronoCoffresValides = Chrono();
unsigned long delaiApresCoffresValides = 1000;
unsigned long delaiDepartSonDrakkar    = 2000;

void setup() {

  // Config DMX
  DmxSimple.usePin(pinDMX);
  DmxSimple.maxChannel(maxChannelDMX);
  
  if (Debug) {
    Serial.begin(115200);
  }

  // initialisation des boutons (~20ms)
  delay(100);

  drakkar.initialiser();
  coffres.initialiser();
}



void loop() {

  // On ne peut (ré)ouvrir les coffres tant que la boule
  // n'est pas (de nouveau) prête à être relachée.

  if (Debug) {
    delay(100);
    Serial.println("Boule presente : ");
    Serial.println((bool)drakkar.boulePresente());
  }

  if (drakkar.boulePresente()) {
    drakkar.eteindreYeux(); 
  } else {
    delay(200);
    return;
  }


  coffres.actualiser();

  // Tous les coffres ont été activés et refermés
  // alors on peut lacher la boule du drakkar.

  // On ne peut réouvrir les coffres tant que la boule
  // n'est pas de nouveau prête à être relachée.

  if (coffres.tousActifsEtFermes()) {
    if (chronoCoffresValides.ready()) {
      chronoCoffresValides.start(); 
    } else if (chronoCoffresValides.get() >= delaiApresCoffresValides) {
      chronoCoffresValides.stop();
      
      coffres.verrouiller();
      drakkar.envoyerSon();
      drakkar.allumerYeux();
      
      delay(delaiDepartSonDrakkar);
      drakkar.envoyerBoule();
  
      coffres.initialiser();    
    }
  } else {
    chronoCoffresValides.stop();
  }


}
