#ifndef Coffre_extensions_h
#define Coffre_extensions_h

#include "Coffre.h"

class Coffre_Feu : public Coffre
{
  public:
    Coffre_Feu(int pinD, int pinO, int pinV, int pinL, int pinS, DmxSimpleClass &DmxSimple, int DmxAdresse, Color couleur);

    void allumerProjecteur(void);
};

#endif
