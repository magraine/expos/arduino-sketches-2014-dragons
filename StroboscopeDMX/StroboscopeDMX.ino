
#include <DmxSimple.h>


void setup() {
  /* Broche utilisée */
  DmxSimple.usePin(3);

  /* Nombre de chaines maximum utilisées. Pour les spot led : 6 */
  DmxSimple.maxChannel(2);

  DmxSimple.write(1, 0);
  DmxSimple.write(2, 0);
}

void loop() {
  int wait = random(2, 20);
  delay(wait * 1000); 
  strob();
}


void strob() {
  // 1 : luminosité
  // 2 : vitesse
  DmxSimple.write(1, 255);
  
  int strob_eclair = random(3, 30);
  DmxSimple.write(2, strob_eclair);

  int duree_eclair = random(10, 800);
  delay(duree_eclair);

  DmxSimple.write(1, 0);
  DmxSimple.write(2, 0);

}
