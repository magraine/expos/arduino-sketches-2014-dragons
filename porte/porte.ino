
// commande du moteur de puissance, via carte
// Dual H-Bridge (RobotBase)
int pinPorte1 = 8; //define I1 port
int pinPorte2 = 9; //define I2 port
int pinPorteVitesse = 10; //define EA(PWM speed regulation)port

// temps d'attente (ms)
unsigned long tempsOuverture = 22 * 1000;
unsigned long tempsFermeture = 22 * 1000;
unsigned long tempsAttenteOuvert = 156; // s
unsigned long tempsAttenteDepartVideo = 1.5 * 1000;

// boutons pour forcer ouverture et fermeture
int pinBtnOuverture = 3;
int pinBtnFermeture = 2;

// capteur de lumière
int pinCapteurLumiere = A0;
int pinLedTemoin  = 13;
int seuilActivation = 600;

// lecteur CD
int pinTelecommande = 4;

void setup() {
  // déclarations pour le pont H
  pinMode(pinPorte1, OUTPUT);
  pinMode(pinPorte2, OUTPUT);
  pinMode(pinPorteVitesse, OUTPUT);
  
  // boutons
  pinMode(pinBtnOuverture, INPUT);
  pinMode(pinBtnFermeture, INPUT);
  
  // lumière
  pinMode(pinCapteurLumiere, INPUT);
  pinMode(pinLedTemoin, OUTPUT);
  
  // telecommande
  pinMode(pinTelecommande, OUTPUT);
  digitalWrite(pinTelecommande, LOW);
  
  couper_porte();
  
  //Serial.begin(9600);
}


void loop() {
  int btnOuverture = digitalRead(pinBtnOuverture);
  int btnFermeture = digitalRead(pinBtnFermeture);
  int valLumiere   = analogRead(pinCapteurLumiere);
  
  //Serial.println(valLumiere);
  
  if (valLumiere >= seuilActivation) 
  {
    digitalWrite(pinLedTemoin, HIGH);
    actionner_video();
    delay(tempsAttenteDepartVideo);
    ouvrir_porte();
    delay(tempsAttenteOuvert * 1000);
    fermer_porte();
    digitalWrite(pinLedTemoin, LOW);
  }
  else if (btnOuverture) 
  {
     actionner_video();
     delay(tempsAttenteDepartVideo);
     ouvrir_porte(); 
  }
  else if (btnFermeture) 
  {
     fermer_porte(); 
  }
  delay(100);
}


