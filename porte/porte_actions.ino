
void ouvrir_porte() {
  actionner_porte(tempsOuverture, false);
}

void fermer_porte() {
  actionner_porte(tempsFermeture, true);
}

/** 
 * Démarre le moteur dans un sens ou dans l'autre
 */
void actionner_porte(int duree, bool anticlockwise) {
  // demarrer le moteur
  digitalWrite(pinPorteVitesse, 255);
  if (anticlockwise) {
    digitalWrite(pinPorte1, HIGH);// DC motor rotates anti-clockwise
    digitalWrite(pinPorte2, LOW);
  } else {
    digitalWrite(pinPorte1, LOW);// DC motor rotates clockwise
    digitalWrite(pinPorte2, HIGH);    
  }
  // temps d'ouverture
  delay(duree);
  
  couper_porte();
}

void couper_porte() {
   // stopper le moteur
  digitalWrite(pinPorte1, LOW);
  digitalWrite(pinPorte2, LOW);
  digitalWrite(pinPorteVitesse, 0);  
}
